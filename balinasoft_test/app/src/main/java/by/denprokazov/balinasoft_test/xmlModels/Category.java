package by.denprokazov.balinasoft_test.xmlModels;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.TextContent;
import com.tickaroo.tikxml.annotation.Xml;

@Xml(name = "category")
public class Category {

    @Attribute(name = "id")
    public String categoryId;

    @TextContent
    public String categoryName;

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }
}
