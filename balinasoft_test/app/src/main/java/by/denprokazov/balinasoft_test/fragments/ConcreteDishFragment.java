package by.denprokazov.balinasoft_test.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import by.denprokazov.balinasoft_test.R;
import by.denprokazov.balinasoft_test.XMLParser;
import by.denprokazov.balinasoft_test.viewModels.ConcreteDishModel;

public class ConcreteDishFragment extends Fragment {

    private String selectedDishId;
    private ConcreteDishModel concreteDishModel;
    private DisplayImageOptions displayImageOptions;
    private String query;
    private AnimateFirstDisplayListener animateFirstListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("concrete_dish");
        getExtraFromPreviousFragment();
        initImageOptions();
        return inflater.inflate(R.layout.concrete_dish_fragment, container, false);
    }

    private void initImageOptions() {
        displayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.common_ic_googleplayservices)
                .showImageForEmptyUri(R.drawable.common_full_open_on_phone)
                .showImageOnFail(R.drawable.cast_ic_notification_connecting)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        XMLParser xmlParser = new XMLParser(getActivity().getApplicationContext());
        TextView dishName = (TextView) view.findViewById(R.id.concrete_dish_name_tv);
        TextView dishCost = (TextView) view.findViewById(R.id.concrete_dish_cost_value_tv);
        TextView dishWeight = (TextView) view.findViewById(R.id.concrete_dish_weight_value_tv);
        TextView dishDescription = (TextView) view.findViewById(R.id.concrete_dish_description_value);
        ImageView dishImage = (ImageView) view.findViewById(R.id.concrete_dish_picture_iv);
        try {
            concreteDishModel = xmlParser.getConcreteDishModel(selectedDishId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        getImage(dishImage);
        fillViews(dishName, dishCost, dishWeight, dishDescription);
    }

    private void getImage(ImageView dishImage) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity().getApplicationContext()).writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
        formatURL();
        ImageLoader.getInstance().displayImage(query,
                dishImage,
                displayImageOptions, animateFirstListener);
    }

    private void fillViews(TextView dishName, TextView dishCost, TextView dishWeight, TextView dishDescription) {
        dishName.setText(concreteDishModel.getDishName());
        dishCost.setText(concreteDishModel.getDishCost());
        dishWeight.setText(concreteDishModel.getDishWeight());
        dishDescription.setText(concreteDishModel.getDishDescription());
    }

    private void getExtraFromPreviousFragment() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            selectedDishId = bundle.getString("dishId");
        }
    }

    private void formatURL() {
        try {
            URL url = new URL(URLDecoder.decode(concreteDishModel.getDishImageUrl()));
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort()
                    ,url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
            query = url.toString();
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
