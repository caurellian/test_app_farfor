package by.denprokazov.balinasoft_test.viewModels;

public class ConcreteDishModel {
    private String concreteDishId;
    private String dishName;
    private String dishWeight;
    private String dishDescription;
    private String dishCost;
    private String dishImageUrl;

    public void setConcreteDishId(String concreteDishId) {
        this.concreteDishId = concreteDishId;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public void setDishWeight(String dishWeight) {
        this.dishWeight = dishWeight;
    }

    public void setDishDescription(String dishDescription) {
        this.dishDescription = dishDescription;
    }

    public void setDishCost(String dishCost) {
        this.dishCost = dishCost;
    }

    public void setDishImageUrl(String dishImageUrl) {
        this.dishImageUrl = dishImageUrl;
    }

    public String getConcreteDishId() {
        return concreteDishId;
    }

    public String getDishName() {
        return dishName;
    }

    public String getDishWeight() {
        return dishWeight;
    }

    public String getDishDescription() {
        return dishDescription;
    }

    public String getDishCost() {
        return dishCost;
    }

    public String getDishImageUrl() {
        return dishImageUrl;
    }
}
