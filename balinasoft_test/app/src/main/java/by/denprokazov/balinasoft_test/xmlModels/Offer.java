package by.denprokazov.balinasoft_test.xmlModels;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.PropertyElement;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.List;

@Xml(name = "offer")
public class Offer {
    @Attribute(name = "id")
    String id;
    @PropertyElement(name = "url")
    public String url;
    @PropertyElement(name = "name")
    public String name;
    @PropertyElement(name = "price")
    public String price;
    @Element(name = "description")
    public Description description;

    @Element(name = "picture")
    public Picture pictrueUrl;

    @PropertyElement(name = "categoryId")
    public String categoryId;

    @Element
    public List<Param> params;

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public Description getDescription() {
        return description;
    }

    public Picture getPictrueUrl() {
        return pictrueUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public List<Param> getParams() {
        return params;
    }
}
