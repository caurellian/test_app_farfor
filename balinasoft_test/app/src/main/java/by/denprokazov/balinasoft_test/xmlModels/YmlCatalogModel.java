package by.denprokazov.balinasoft_test.xmlModels;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.Element;
import com.tickaroo.tikxml.annotation.Path;
import com.tickaroo.tikxml.annotation.PropertyElement;
import com.tickaroo.tikxml.annotation.Xml;

import java.util.List;

@Xml(name = "yml_catalog")
public class YmlCatalogModel {
    @Attribute(name = "date")
    String date;

    @Path("shop/categories")
    @Element
    List<Category> categories;

    @Path("shop/offers")
    @Element
    List<Offer> offers;

    public List<Category> getCategories() {
        return categories;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    
}
