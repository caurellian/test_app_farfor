package by.denprokazov.balinasoft_test.viewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import by.denprokazov.balinasoft_test.R;
import by.denprokazov.balinasoft_test.xmlModels.Category;

public class CategoriesAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<Category> categories;

    public CategoriesAdapter(Context context, List<Category> categories) {
        super(context, R.layout.main_list_fragment);
        this.context = context;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    public String getCategoryId(int position) {
        return categories.get(position).getCategoryId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.category_list_item, null, true);
            viewHolder = new ViewHolder();
            viewHolder.categoryName = (TextView) view.findViewById(R.id.category_name_tv);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.categoryName.setText(categories.get(position).getCategoryName());

        return view;
    }

    static class ViewHolder {
        TextView categoryName;
        ImageView categoryImage;
    }
}
