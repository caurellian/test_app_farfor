package by.denprokazov.balinasoft_test.viewAdapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import by.denprokazov.balinasoft_test.R;
import by.denprokazov.balinasoft_test.viewModels.DishListViewModel;

public class DishListAdapter extends ArrayAdapter<String> {
    private final List<DishListViewModel> dishListViewModel;
    private final Context context;
    private final DisplayImageOptions displayImageOptions;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    private String query;

    public DishListAdapter(Context context, List<DishListViewModel> dishListViewModels) {
        super(context, R.layout.main_list_fragment);
        this.dishListViewModel = dishListViewModels;
        this.context = context;

        displayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.common_ic_googleplayservices)
                .showImageForEmptyUri(R.drawable.common_full_open_on_phone)
                .showImageOnFail(R.drawable.cast_ic_notification_connecting)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();
    }


    @Override
    public int getCount() {
        return dishListViewModel.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.dish_list_item, null, true);
            viewHolder = new ViewHolder();
            initViewHolder(viewHolder, view);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        getImage(position, viewHolder);
        fillViews(position, viewHolder);
        return view;
    }

    private void getImage(int position, ViewHolder viewHolder) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
        formatURL(position);
        ImageLoader.getInstance().displayImage(query,
                viewHolder.dishIconIV,
                displayImageOptions, animateFirstListener);
    }

    public String getDishId(int position) {
        return dishListViewModel.get(position).getDishListId();
    }

    private void fillViews(int position, ViewHolder viewHolder) {
        viewHolder.dishNameTV.setText(dishListViewModel.get(position).getDishName());
        viewHolder.dishCostTV.setText(dishListViewModel.get(position).getDishCost());
        viewHolder.dishWeightTV.setText(dishListViewModel.get(position).getDishWeight());

    }

    private void initViewHolder(ViewHolder viewHolder, View view) {
        viewHolder.dishNameTV = (TextView) view.findViewById(R.id.dish_name_tv);
        viewHolder.dishWeightTV = (TextView) view.findViewById(R.id.dish_weight_tv);
        viewHolder.dishCostTV = (TextView) view.findViewById(R.id.dish_cost_tv);
        viewHolder.dishIconIV = (ImageView) view.findViewById(R.id.dish_image_iv);
        view.setTag(viewHolder);
    }

    private void formatURL(int position) {
        try {
            URL url = new URL(URLDecoder.decode(dishListViewModel.get(position).getDishIconUrl()));
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort()
                    ,url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
            query = url.toString();
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    static class ViewHolder {
        ImageView dishIconIV;
        TextView dishNameTV;
        TextView dishWeightTV;
        TextView dishCostTV;
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
