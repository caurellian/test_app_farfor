package by.denprokazov.balinasoft_test.xmlModels;

import com.tickaroo.tikxml.annotation.Attribute;
import com.tickaroo.tikxml.annotation.TextContent;
import com.tickaroo.tikxml.annotation.Xml;

@Xml(name = "param")
public class Param {

    @Attribute(name = "name")
    public String name;

    @TextContent
    public String paramVal;
}
