package by.denprokazov.balinasoft_test;

import android.content.Context;
import android.content.res.AssetManager;

import com.tickaroo.tikxml.TikXml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.balinasoft_test.viewModels.ConcreteDishModel;
import by.denprokazov.balinasoft_test.viewModels.DishListViewModel;
import by.denprokazov.balinasoft_test.xmlModels.Category;
import by.denprokazov.balinasoft_test.xmlModels.Offer;
import by.denprokazov.balinasoft_test.xmlModels.YmlCatalogModel;
import okio.Buffer;

public class XMLParser {
    private final Context context;


    public XMLParser(Context context) {
        this.context = context;
    }

    public List<Category> getCategoryModels() throws IOException {
        List<Category> categories = new ArrayList<>();
        categories = tikxmlParse().getCategories();
        return categories;
    }

    public List<DishListViewModel> getDishListModels(String selectedCategoryId) throws IOException {
        List<Offer> offers;
        List<DishListViewModel> dishListViewModels = new ArrayList<>();
        offers = tikxmlParse().getOffers();
        for (int i = 0; i < offers.size(); i++) {
            if(offers.get(i).getCategoryId().equals(selectedCategoryId)) {
                DishListViewModel dishListViewModel = new DishListViewModel();
                dishListViewModel.setDishName(offers.get(i).getName());
                dishListViewModel.setDishListId(offers.get(i).getId());
                dishListViewModel.setDishCost(offers.get(i).getPrice());
                dishListViewModel.setDishIconUrl(offers.get(i).getPictrueUrl().pictureURL);
                for (int n = 0; n < offers.get(i).getParams().size(); n++) {
                    if(offers.get(i).getParams().get(n).name.equals("Вес")) {
                        dishListViewModel.setDishWeight(offers.get(i).getParams().get(n).paramVal);
                    }
                }
                dishListViewModels.add(dishListViewModel);
            }
        }
        return dishListViewModels;
    }

    public ConcreteDishModel getConcreteDishModel(String selectedDishId) throws IOException {
        List<Offer> offers;
        List<ConcreteDishModel> concreteDishModels = new ArrayList<>();
        offers = tikxmlParse().getOffers();
        ConcreteDishModel concreteDishModel = new ConcreteDishModel();
        for (int i = 0; i < offers.size(); i++) {
            if(offers.get(i).getId().equals(selectedDishId)) {
                concreteDishModel.setDishName(offers.get(i).getName());
                concreteDishModel.setDishCost(offers.get(i).getPrice());
                concreteDishModel.setDishImageUrl(offers.get(i).getPictrueUrl().pictureURL);
                for (int n = 0; n < offers.get(i).getParams().size(); n++) {
                    if(offers.get(i).getParams().get(n).name.equals("Вес")) {
                        concreteDishModel.setDishWeight(offers.get(i).getParams().get(n).paramVal);
                    }
                }
                concreteDishModel.setDishDescription(offers.get(i).getDescription().descriprtionValue);
            }
        }
        return concreteDishModel;
    }

    public YmlCatalogModel tikxmlParse() throws IOException {
        String xmlMockString = getXmlMockString();
        return new TikXml.Builder().exceptionOnUnreadXml(true).build()
                .read(new Buffer().writeUtf8(xmlMockString), YmlCatalogModel.class);
    }

    private String getXmlMockString() throws IOException {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open("xml/xml_mock.xml");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int len;
        while ((len = inputStream.read(buf)) != -1) {
            outputStream.write(buf, 0, len);
        }
        outputStream.close();
        inputStream.close();
        return  outputStream.toString();
    }
}
