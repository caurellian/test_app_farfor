package by.denprokazov.balinasoft_test.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

import by.denprokazov.balinasoft_test.R;
import by.denprokazov.balinasoft_test.XMLParser;
import by.denprokazov.balinasoft_test.viewAdapters.CategoriesAdapter;
import by.denprokazov.balinasoft_test.viewModels.CategoryModel;
import by.denprokazov.balinasoft_test.xmlModels.Category;

public class CategoryFragment extends Fragment {

    private List<Category> categoryModels;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(getString(R.string.categories_label_string));
        return inflater.inflate(R.layout.main_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        XMLParser xmlParser = new XMLParser(getActivity().getApplicationContext());

        CategoriesAdapter categoriesAdapter = null;
        try {
            categoryModels = xmlParser.getCategoryModels();
            categoriesAdapter = new CategoriesAdapter(getActivity()
                    , categoryModels);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ListView categoriesListView = (ListView) view.findViewById(R.id.main_list_view);
        categoriesListView.setAdapter(categoriesAdapter);
        final CategoriesAdapter finalCategoriesAdapter = categoriesAdapter;
        categoriesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("categoryId", finalCategoriesAdapter.getCategoryId(position));
                DishListFragment dishListFragment = new DishListFragment();
                dishListFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, dishListFragment)
                        .addToBackStack("categoryDishes")
                        .commit();
            }
        });
    }
}
