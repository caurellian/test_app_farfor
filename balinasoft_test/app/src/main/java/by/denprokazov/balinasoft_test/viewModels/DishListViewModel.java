package by.denprokazov.balinasoft_test.viewModels;

public class DishListViewModel {
    private String dishListId;
    private String dishName;
    private String dishIconUrl;
    private String dishWeight;
    private String dishCost;

    public String getDishListId() {
        return dishListId;
    }

    public String getDishName() {
        return dishName;
    }

    public String getDishIconUrl() {
        return dishIconUrl;
    }

    public String getDishWeight() {
        return dishWeight;
    }

    public String getDishCost() {
        return dishCost;
    }

    public void setDishListId(String dishListId) {
        this.dishListId = dishListId;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public void setDishIconUrl(String dishIconUrl) {
        this.dishIconUrl = dishIconUrl;
    }

    public void setDishWeight(String dishWeight) {
        this.dishWeight = dishWeight;
    }

    public void setDishCost(String dishCost) {
        this.dishCost = dishCost;
    }
}
