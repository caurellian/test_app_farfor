package by.denprokazov.balinasoft_test.xmlModels;

import com.tickaroo.tikxml.annotation.TextContent;
import com.tickaroo.tikxml.annotation.Xml;

@Xml(name = "picture")
public class Picture {
    @TextContent
    public String pictureURL;
}
