package by.denprokazov.balinasoft_test.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import by.denprokazov.balinasoft_test.R;
import by.denprokazov.balinasoft_test.XMLParser;
import by.denprokazov.balinasoft_test.viewAdapters.DishListAdapter;

public class DishListFragment extends Fragment {
    private String selectedCategoryId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Category_id_name");
        getExtraFromPreviousFragment();
        return inflater.inflate(R.layout.main_list_fragment, container, false);
    }

    private void getExtraFromPreviousFragment() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            selectedCategoryId = bundle.getString("categoryId");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        XMLParser xmlParser = new XMLParser(getActivity().getApplicationContext());
        ListView listView = (ListView) view.findViewById(R.id.main_list_view);
        DishListAdapter dishListAdapter = null;
        try {
            dishListAdapter = new DishListAdapter(getActivity(), xmlParser.getDishListModels(selectedCategoryId));
        } catch (IOException e) {
            e.printStackTrace();
        }
        listView.setAdapter(dishListAdapter);
        final DishListAdapter finalDishListAdapter = dishListAdapter;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("dishId", finalDishListAdapter.getDishId(position));
                ConcreteDishFragment concreteDishFragment = new ConcreteDishFragment();
                concreteDishFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, concreteDishFragment).addToBackStack("dishConcreteDish")
                        .commit();
            }
        });
    }
}
