package by.denprokazov.balinasoft_test;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import by.denprokazov.balinasoft_test.fragments.CategoryFragment;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initNavigationDrawer();
        initUI();
    }

    private void initUI() {
        CategoryFragment categoryFragment = new CategoryFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, categoryFragment);
        fragmentTransaction.commit();
    }

    private void initNavigationDrawer() {
        String[] sidebarArray = {"Каталог", "Контакты"};
        ArrayAdapter<String> sidebarListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sidebarArray);

        android.support.v7.widget.Toolbar mainToolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mainToolbar);

        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView sideLV = (ListView) findViewById(R.id.side_list_view);
        sideLV.setAdapter(sidebarListAdapter);
        ActionBarDrawerToggle sidebarTooggle = new ActionBarDrawerToggle(
                this, drawerLayout, mainToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        sidebarTooggle.syncState();
        drawerLayout.setDrawerListener(sidebarTooggle);
        sideLV.bringToFront();

        sideLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String navDrawerCall = "navDrawerCall";
                if (position == 0) {
                    drawerLayout.closeDrawers();
                    CategoryFragment categoryFragment = new CategoryFragment();
                    replaceFragmentBy(categoryFragment, navDrawerCall);
                }
                if (position == 1) {
                    Toast.makeText(getApplicationContext(), "Settings will arrive soon", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void replaceFragmentBy(Fragment newFragment, String tag) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, newFragment);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
