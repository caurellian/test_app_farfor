package by.denprokazov.balinasoft_test.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

public class ImageLoaderDeprecated extends AsyncTask<String, String, Bitmap> {
    private final ProgressBar progressBar;
    private Bitmap bitmap;
    private final WeakReference<ImageView> viewWeakReference;
    private ImageView imageView;

    public ImageLoaderDeprecated(ImageView imageView, ProgressBar progressBar) {
        viewWeakReference = new WeakReference<ImageView>(imageView);
        this.progressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        imageView = viewWeakReference.get();
        imageView.setVisibility(View.GONE);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(params[0]).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if(imageView != null) {
            progressBar.setVisibility(View.GONE);
            imageView.setImageBitmap(bitmap);
            imageView.setVisibility(View.VISIBLE);
        } else {
            Log.d("Internet exception", "Failed to load image");
        }
    }
}
